// ======== highlight and refresh window

const webdriver = require("selenium-webdriver"),
  By = webdriver.By,
  until = webdriver.until,
  Key = webdriver.Key;
const chrome = require("selenium-webdriver/chrome");
const path = require("chromedriver").path;
const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
const driver = new webdriver.Builder()
  .withCapabilities(webdriver.Capabilities.chrome())
  .build();
const actions = driver.actions({ async: true });

async function highlight(el) {
  const bg = await el.getCssValue("backgroundColor");
  console.log(bg);
  await driver.executeScript("arguments[0].style.backgroundColor = 'red'", el);
  await driver.sleep(1000);
  await driver.executeScript(
    "arguments[0].style.backgroundColor = ' " + bg + " ' ",
    el
  );
}

(async function example() {
  try {
    await driver.get("https://mail.yandex.by/");
    await driver
      .findElement(
        By.className(
          "button2 button2_size_mail-big button2_theme_mail-white button2_type_link HeadBanner-Button HeadBanner-Button-Enter with-shadow"
        )
      )
      .click();
    await driver
      .findElement(By.id("passp-field-login"))
      .sendKeys("taDmitryS", Key.RETURN);
    await driver.wait(until.elementLocated(By.id("passp-field-passwd")), 10000);
    await driver
      .findElement(By.id("passp-field-passwd"))
      .sendKeys("test12345", Key.RETURN);
    await driver.wait(
      until.elementLocated(
        By.className("mail-ComposeButton js-main-action-compose")
      ),
      10000
    );
    const el1 = await driver.findElement(By.className("textinput__control"));
    await el1.sendKeys("SOME TEXT FOR EXAMPLE");
    await highlight(el1);
    await driver.sleep(3000);
    const el2 = await driver.findElement(
      By.className(
        "yandex-header__logo-base yandex-header__logo-base_lang_ru count-me"
      )
    );
    await highlight(el2);
    await driver.sleep(3000);
    // refresh the window
    await driver.executeScript("history.go(0)");
    await driver.sleep(5000);
  } finally {
    await driver.quit();
  }
})();
