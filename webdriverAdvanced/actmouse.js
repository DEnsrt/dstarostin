// ======= actions mouse and keyboard

const webdriver = require("selenium-webdriver"),
  By = webdriver.By,
  until = webdriver.until,
  Key = webdriver.Key;
const chrome = require("selenium-webdriver/chrome");
const path = require("chromedriver").path;
const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
const driver = new webdriver.Builder()
  .withCapabilities(webdriver.Capabilities.chrome())
  .build();
const actions = driver.actions({ async: true });
(async function example() {
  try {
    await driver.get("https://mail.yandex.by/");
    await driver
      .findElement(
        By.className(
          "button2 button2_size_mail-big button2_theme_mail-white button2_type_link HeadBanner-Button HeadBanner-Button-Enter with-shadow"
        )
      )
      .click();
    await driver
      .findElement(By.id("passp-field-login"))
      .sendKeys("taDmitryS", Key.RETURN);
    await driver.wait(until.elementLocated(By.id("passp-field-passwd")), 10000);
    await driver
      .findElement(By.id("passp-field-passwd"))
      .sendKeys("test12345", Key.RETURN);
    await driver.wait(
      until.elementLocated(
        By.className("mail-ComposeButton js-main-action-compose")
      ),
      10000
    );

    //======= context click

    let searchBtn = driver.findElement(
      By.className("mail-NestedList-Item-Name")
    );

    await actions.contextClick(searchBtn).perform();
    await driver.sleep(5000);

    // ====== keyDown and keyUp

    const el1 = (await driver).findElement(
      By.xpath(
        "/html/body/div[2]/div[5]/div/div[3]/div[3]/div[2]/div[5]/div[1]/div/div/div[2]/div/div[1]/div/div"
      )
    );
    const el2 = (await driver).findElement(
      By.xpath(
        "/html/body/div[2]/div[5]/div/div[3]/div[3]/div[2]/div[5]/div[1]/div/div/div[2]/div/div[2]"
      )
    );

    await driver
      .actions()
      .keyDown(Key.SHIFT)
      .click(el1)
      .click(el2)
      .keyUp(Key.SHIFT)
      .perform();
    await driver.sleep(5000);
  } finally {
    await driver.quit();
  }
})();
