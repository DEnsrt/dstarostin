// ============== log to the email and send a mail

const webdriver = require("selenium-webdriver"),
  By = webdriver.By,
  until = webdriver.until,
  Key = webdriver.Key;
const chrome = require("selenium-webdriver/chrome");
const path = require("chromedriver").path;
const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);
const driver = new webdriver.Builder()
  .withCapabilities(webdriver.Capabilities.chrome())
  .build();
const actions = driver.actions({ async: true });
(async function example() {
  try {
    await driver.get("https://mail.yandex.by/");
    await driver
      .findElement(
        By.className(
          "button2 button2_size_mail-big button2_theme_mail-white button2_type_link HeadBanner-Button HeadBanner-Button-Enter with-shadow"
        )
      )
      .click();
    await driver
      .findElement(By.id("passp-field-login"))
      .sendKeys("taDmitryS", Key.RETURN);
    await driver.wait(until.elementLocated(By.id("passp-field-passwd")), 10000);
    await driver
      .findElement(By.id("passp-field-passwd"))
      .sendKeys("test12345", Key.RETURN);
    await driver.wait(
      until.elementLocated(
        By.className("mail-ComposeButton js-main-action-compose")
      ),
      10000
    );

    await driver
      .findElement(By.className("mail-ComposeButton js-main-action-compose"))
      .click();
    await driver.wait(
      until.elementLocated(By.className("composeYabbles")),
      10000
    );

    await driver
      .findElement(By.className("composeYabbles"))
      .sendKeys("taDmitryS@yandex.by", Key.RETURN);
    await driver
      .findElement(By.className("composeTextField ComposeSubject-TextField"))
      .sendKeys("test text", Key.RETURN);
    await driver
      .findElement(
        By.className(
          "cke_wysiwyg_div cke_reset cke_enable_context_menu cke_editable cke_editable_themed cke_contents_ltr"
        )
      )
      .sendKeys("test", Key.RETURN);
    await driver
      .findElement(
        By.className(
          "control button2 button2_view_default button2_tone_default button2_size_l button2_theme_action button2_pin_circle-circle ComposeControlPanelButton-Button ComposeControlPanelButton-Button_action"
        )
      )
      .click();
    await driver.wait(until.titleIs("webdriver - Google Search"), 10000);
  } finally {
    await driver.quit();
  }
})();
