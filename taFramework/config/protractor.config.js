exports.config = {
  directConnect: true,

  framework: "mocha",

  specs: ["../specs/*.js"],

  capabilities: {
    shardTestFiles: true,
    maxInstances: 1,
    browserName: "chrome",
    chromeOptions: {
      args: [
        "--incognito",
        "--no-sandbox",
        "--disable-web-security",
        "--allow-running-insecure-content",
        "--disable-gpu",
        "--start-maximized",
        "disable-extensions",
        "--disable-infobars",
      ],
    },
  },

  baseUrl: "https://mail.yandex.by/",

  onPrepare: async function () {
    await browser.waitForAngularEnabled(false);

    await browser.manage().window().maximize();
  },
  // restartBrowserBetweenTests: true,
  mochaOpts: {
    reporter: "spec",
    timeout: 70000,
  },
};
