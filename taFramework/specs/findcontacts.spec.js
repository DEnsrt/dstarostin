const { browser } = require("protractor");
const protractor = require("protractor");
const Key = protractor.Key;
const { by, element, $ } = require("protractor");
const { expect } = require("chai");
const {
  validLogin,
  validPassword,
  defaultTimeoutMs,
} = require("../utils/constants");
const LoginPage = require("../pages/login_page");
const WelcomePage = require("../pages/welcome_page");
const MailBox = require("../pages/mailBox");
const mailBox = require("../pages/mailBox");

describe("Find contacts field", async () => {
  before(async () => {
    //await browser.restart();
    await browser.sleep(5000);
    await browser.get("https://mail.yandex.by/");
    await WelcomePage.clickEnterButton();
    await LoginPage.performLogin(validLogin, validPassword);
    await browser.sleep(defaultTimeoutMs);
  });

  it("should find the contacts field", async () => {
    element(by.xpath("//span[contains(text(),'Контакты')]"));
    element(
      by.cssContainingText(
        "a.yandex-header__nav-link.yandex-header__nav-link_active_no.count-me:nth-child(1) > span",
        "Контакты"
      )
    );
  });
});
