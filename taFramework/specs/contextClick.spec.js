const { browser } = require("protractor");
const protractor = require("protractor");
const Key = protractor.Key;
const { by, element, $ } = require("protractor");
const { expect } = require("chai");
const {
  validLogin,
  validPassword,
  defaultTimeoutMs,
} = require("../utils/constants");
const LoginPage = require("../pages/login_page");
const WelcomePage = require("../pages/welcome_page");

describe("Yandex mail with valid credentials", () => {
  before(async () => {
    await browser.get("https://mail.yandex.by/");
    await WelcomePage.clickEnterButton();
    await LoginPage.performLogin(validLogin, validPassword);
    await browser.sleep(defaultTimeoutMs);
  });

  it("should select first letter with button SHIFT", async () => {
    const element1 = element(
      by.xpath(
        "//*[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_sender js-message-snippet-sender']"
      )
    );
    await browser
      .actions()
      .keyDown(Key.SHIFT)
      .click(element1)
      .keyUp(Key.SHIFT)
      .perform();
    await browser.sleep(defaultTimeoutMs);
  });

  it("should click on the element New mail", async () => {
    const searchBtn = element(by.className("mail-ComposeButton-Text"));
    await browser.actions().mouseMove(searchBtn).click(searchBtn).perform();
    await browser.sleep(defaultTimeoutMs);
  });
  it("should click on the close new email", async () => {
    const closeBtn = element(
      by.xpath(
        "//div[@class='composeHeader composeHeader_expanded composeHeader_activable']//div[@class='ControlButton ControlButton_button_close']//button[@type='button']"
      )
    );
    await browser.actions().click(closeBtn).perform();
    await browser.sleep(3000);
  });
  it("should find search field and eter text: test!", async () => {
    const el1 = element(by.xpath("//input[@placeholder='Поиск']"));
    const el2 = element(
      by.className(
        "control button2 button2_view_default button2_tone_mail-suggest-themed button2_size_n button2_theme_normal button2_pin_clear-round button2_type_submit search-input__form-button"
      )
    );
    await browser.actions().click(el1).sendKeys("test!").perform();
    await browser.sleep(defaultTimeoutMs);
    await browser.actions().click(el2).perform();
    browser.sleep(3000);
  });

  it("should exit from accaunt", async () => {
    element(
      by.xpath(
        "//a[@class='user-account user-account_left-name user-account_has-ticker_yes user-account_has-accent-letter_yes count-me legouser__current-account legouser__current-account i-bem']//img[@class='user-pic__image']"
      )
    ).click();
    element(
      by.xpath("//span[contains(text(),'Выйти из сервисов Яндекса')]")
    ).click();
  });
});
