const { by, element, $, browser } = require("protractor");
const BasePage = require("./base_page");
const DriverUtils = require("../utils/driver_utils");

class WelcomePage extends BasePage {
  constructor() {
    super();
  }
  get enterButton() {
    return element(
      by.className(
        "control button2 button2_view_classic button2_size_mail-big button2_theme_mail-white button2_type_link HeadBanner-Button HeadBanner-Button-Enter with-shadow"
      )
    );
    //by.xpath("//span[text()='Войти']/..")
  }
  clickEnterButton = async () => {
    await DriverUtils.clickElement(this.enterButton);
  };
}
module.exports = new WelcomePage();
