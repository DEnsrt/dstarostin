const { $ } = require("protractor");
const BasePage = require("./base_page");
const DriverUtils = require("../utils/driver_utils");

class MailBox extends BasePage {
  constructor() {
    super();
  }

  get username() {
    return $(".user-account_left-name span.user-account__name");
  }

  getUserName = async () => {
    await DriverUtils.waitForElementVisibility(this.username);
    await browser.sleep(2000);
    const username = await this.username.getText();
    return username;
  };
}

module.exports = new MailBox();
