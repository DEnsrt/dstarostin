const { by, element, $, browser } = require("protractor");
const BasePage = require("./base_page");
const DriverUtils = require("../utils/driver_utils");

class LoginPage extends BasePage {
  constructor() {
    super();
  }
  get loginIput() {
    return element(by.name("login"));
  }
  get passwordInput() {
    return element(by.name("passwd"));
  }
  get submitButton() {
    return element(by.css("button[type=submit]"));
  }
  get errorMessage() {
    return $(".Textinput-Hint_state_error");
  }

  performLogin = async (login, password) => {
    await DriverUtils.enterText(this.loginIput, login);
    await DriverUtils.clickElement(this.submitButton);
    await DriverUtils.enterText(this.passwordInput, password);
    await DriverUtils.clickElement(this.submitButton);
  };
  getErrorMessage = async () => {
    await DriverUtils.waitForElementPresence(this.errorMessage);
    await browser.sleep(2000);
    const errorMessage = await this.errorMessage.getText();
    return errorMessage;
  };
}
module.exports = new LoginPage();
