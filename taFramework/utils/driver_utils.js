const { defaultTimeoutMs } = require("./constants");
const { browser, protractor } = require("protractor");
const EC = browser.ExpectedConditions;

class DriverUtils {
  static clickElement = async (element) => {
    await browser.wait(EC.elementToBeClickable(element), defaultTimeoutMs);
    await element.click();
  };
  static getElementText = async (element) => {
    await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
    return await element.getText();
  };
  /* static clickOutput = async (element) => {
    await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
    const isClickable = EC.elementToBeClickable(element);
    
  }; */

  static enterText = async (element, text) => {
    await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
    await element.sendKeys(text);
  };
  static elementIsCurrentlyPresent = async (element) => {
    await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
    const isPresent = await element.isPresent();
    return isPresent;
  };
  static waitForElementVisibility = async (element) => {
    await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
    return await element.isDisplayed();
  };
  static waitForElementPresence = async (element) => {
    await browser.wait(EC.presenceOf(element), defaultTimeoutMs);
  };
  static waitForElementAbsence = async (element) => {
    await browser.wait(EC.visibilityOf(element), defaultTimeoutMs);
    return this.elementIsCurrentlyPresent(element);
  };
}

module.exports = DriverUtils;
