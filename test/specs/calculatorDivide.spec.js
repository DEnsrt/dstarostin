const { Calculator } = require("../../ut/calculator");
const chai = require("chai");
const spies = require("chai-spies");
const { expect } = require("chai");
chai.use(spies);

describe("Calculator Divide", () => {
  let calculator, spy;
  beforeEach(() => {
    calculator = new Calculator();
    spy = chai.spy.on(calculator, "Calculator Divide");
  });

  afterEach(() => {
    calculator = null;
  });

  it("should return 2 when called with number 10, 5", function () {
    expect(calculator.divide(10, 5)).to.be.equal(2);
  });

  it("should return -5 when called with number 50, -10", function () {
    expect(calculator.divide(50, -10)).to.be.equal(-5);
  });

  it("should return with error", function () {
    const callWithError = () => calculator.divide(10, 0);
    expect(callWithError).to.throw("Can't divide on 0!");
  });

  it("should return with error", function () {
    const callWithError = () => calculator.divide(10, "str");
    expect(callWithError).to.throw("NaN");
  });
});
