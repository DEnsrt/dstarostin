const { Calculator } = require("../../ut/calculator");
const chai = require("chai");
const spies = require("chai-spies");
const { expect } = require("chai");
chai.use(spies);

describe("Calculator Plus", () => {
  let calculator, spy;
  beforeEach(() => {
    calculator = new Calculator();
    spy = chai.spy.on(calculator, "Calculator Plus");
  });

  afterEach(() => {
    calculator = null;
  });

  it("should return 6 when called with number 4,2", function () {
    expect(calculator.plus(4, 2)).to.be.equal(6);
  });

  it("should return -5 when called with number -10, 5", function () {
    expect(calculator.plus(-10, 5)).to.be.equal(-5);
  });

  it("should return -30 when called with number -10, -20", function () {
    expect(calculator.plus(-10, -20)).to.be.equal(-30);
  });

  it("should return with error", function () {
    const callWithError = () => calculator.plus(1, "two");
    expect(callWithError).to.throw("NaN");
  });

  
  /* it("should return 362880 when called with number 9", () => {
    expect(factorial.factorialShort(9)).to.be.equal(362880);
  });

  it("should throw an error if provided with a negative number", () => {
    const callWithError = () => factorial.factorialShort(-1);
    expect(spy).to.be.a.spy;
    expect(callWithError).to.throw("Negative number!");
  });

  it("should be called twice when provided with 2", () => {
    factorial.factorialShort(2);
    expect(spy).to.have.been.called(2);
  });

  it("should return 1 when provided 1", () => {
    expect(factorial.factorialShort(1)).to.be.equal(1);
  }); */
});
