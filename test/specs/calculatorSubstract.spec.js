const { Calculator } = require("../../ut/calculator");
const chai = require("chai");
const spies = require("chai-spies");
const { expect } = require("chai");
chai.use(spies);

describe("Calculator Substract", () => {
  let calculator, spy;
  beforeEach(() => {
    calculator = new Calculator();
    spy = chai.spy.on(calculator, "Calculator Substract");
  });

  afterEach(() => {
    calculator = null;
  });

  it("should return 60 when called with number 78, 18", function () {
    expect(calculator.substract(78, 18)).to.be.equal(60);
  });

  it("should return -5 when called with number 10, 15", function () {
    expect(calculator.substract(10, 15)).to.be.equal(-5);
  });

  it("should return -50 when called with number -33, 17", function () {
    expect(calculator.substract(-33, 17)).to.be.equal(-50);
  });

  it("should return with error", function () {
    const callWithError = () => calculator.substract(1, "two");
    expect(callWithError).to.throw("NaN");
  });
});
