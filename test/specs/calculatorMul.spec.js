const { Calculator } = require("../../ut/calculator");
const chai = require("chai");
const spies = require("chai-spies");
const { expect } = require("chai");
chai.use(spies);

describe("Calculator Multiply", () => {
  let calculator, spy;
  beforeEach(() => {
    calculator = new Calculator();
    spy = chai.spy.on(calculator, "Calculator Multiply");
  });

  afterEach(() => {
    calculator = null;
  });

  it("should return 14 when called with number 7, 2", function () {
    expect(calculator.multiply(7, 2)).to.be.equal(14);
  });

  it("should return -20 when called with number -10, 2", function () {
    expect(calculator.multiply(-10, 2)).to.be.equal(-20);
  });

  it("should return 0 when called with number 10, 0", function () {
    expect(calculator.multiply(10, 0)).to.be.equal(0);
  });

  it("should return with error if provided with a string", function () {
    const callWithError = () => calculator.multiply(10, "string");
    expect(callWithError).to.throw("NaN");
  });

  
});
