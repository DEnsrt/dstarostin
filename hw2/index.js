console.log("===========task1==========");
const task1 = require("./task1");
console.log(task1.newArr());

console.log("===========task2.1==========");
const task2 = require("./task2");
console.log(task2.cityFilter("Mogilev"));


console.log("===========task2.2==========");

console.log(task2.ageFilter(20));

console.log("===========task2.3==========");
console.log(task2.namesFilter());

console.log("===========task2.4==========");
console.log(task2.marriedStatus(false));

console.log("===========task2.5==========");
console.log(task2.ageSexFilter(
    "Male",
    20
));

console.log("===========task2.6==========");
console.log(task2.addStudent(
    "Tihon",
    21,
    "Male",
    true,
    "Mogilev"
));

console.log("===========task2.7==========");
console.log(task2.uniqueCity());