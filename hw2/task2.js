const students = [
    {
        "name": "Vasya Pupkin",
        "age": 17,
        "sex": "Male",
        "isMarried": false,
        "city": "Mogilev"
    },
    {
        "name": "Zoya Petrovna",
        "age": 23,
        "sex": "Female",
        "isMarried": true,
        "city": "Mogilev"
    },
    {
        "name": "Petr Ivanov",
        "age": 30,
        "sex": "Male",
        "isMarried": true,
        "city": "Minsk"
    },
    {
        "name": "Vitali Ivanov",
        "age": 19,
        "sex": "Male",
        "isMarried": false,
        "city": "Vitebsk"
    },
    {
        "name": "Lavrenti Sakalov",
        "age": 20,
        "sex": "Male",
        "isMarried": true,
        "city": "Brest"
    },
    {
        "name": "Olga Sakalova",
        "age": 23,
        "sex": "Female",
        "isMarried": true,
        "city": "Grodno"
    }
]// 1 task
function cityFilter(cit) {
    const cities = students.filter((el) => el.city === cit)
    return cities
}


// 2 task
function ageFilter(ages) {
    const adult = students.filter((el) => el.age <= ages);
    return adult
}


// 3 task
function namesFilter() {
    const names = students.
        sort((a, b) => {
            const nameA = a.name,
                nameB = b.name;
            if (nameA < nameB)
                return -1
            if (nameA > nameB)
                return 1;
            return 0
        }).
        map((student) => `${student.name}`);
    return names
}


// Task4
function marriedStatus(status) {
    const married = students.filter((student) => (student.sex === "Female" && student.isMarried === status));
    return married
}


// Task5
function ageSexFilter(se, ag) {
    const ageSex = students.filter((stud) => stud.sex == se && stud.age == ag);
    return ageSex
}


// Task6
function addStudent(nam, ag, se, isMar, cit) {
    students.unshift({
        "name": nam,
        "age": ag,
        "sex": se,
        "isMarried": isMar,
        "city": cit
    });
    return students
}


// Task7
function uniqueCity() {

    const result = [],
        a = students.map((student) => `${student.city}`);
    for (const stc of a) {
        if (!result.includes(stc)) {
            result.push(stc);
        }
    }

    return result;
}
module.exports = {ageFilter,
    cityFilter,
    namesFilter,
    marriedStatus,
    ageSexFilter,
    addStudent,
    uniqueCity}