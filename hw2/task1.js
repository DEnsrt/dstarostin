function newArr() {
    const array = [
            1,
            "3",
            "hello",
            28,
            {"hey": "day"},
            "hope",
            111
        ],
        arr = [];
    if (array.length % 2 === 1)
        array.push(null);
    array.forEach((elem, i) => {
        if (i % 2 === 0)
            arr.push(`${array[i]}:${array[i + 1]}`);
    });
    return arr;
}
module.exports = {newArr};