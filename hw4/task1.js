class Card {
    constructor(balance, name) {
        this.name = name || process.env.USER;
        this.balance = balance || 0;
    };
    refill(amount) {
        this.amount = amount;
        this.balance = this.balance + amount;
        return ('your new balance: ' + this.balance);
    };
    withdrawal(amount) {
        this.amount = amount;
        this.balance = this.balance - amount;
        return ('your new balance: ' + this.balance);
    }
};

class CreditCard extends Card {

};

class DebitCard extends Card {
    withdrawal(amount) {
        if ((this.balance - amount) >= 0) {
            return 'your balance after withdrawal: ' + (this.balance - amount);
        } else {
            console.log('ERROR! not enough money to withdrawal: ' + `${amount}`);
            return ('your balance: ' + this.balance);
        }
    }

};



module.exports = { Card, CreditCard, DebitCard };