class Track {

    constructor(trackName, time, explicit) {
        this.trackName = trackName;
        this.time = time;
        this.explicit = explicit;
        this.album = 0;
        this.artist = 0;
    };

};

module.exports = Track;