

class Album {
    constructor(albumDate, albumName, artist) {

        this.albumName = albumName;
        this.albumDate = albumDate;
        this.artist = null;

    };
    trackList = [];

    addTrack(track) {
        this.trackList.push(track);
        track.artist = this.artist;
        track.album = this.album;
        this.trackList[this.trackList.length - 1].album = this;


    }
}
module.exports = Album;