/* console.log('===========task1==========');
const task1 = require('./task1');
const credit = new task1.CreditCard(1000, 'Belka Iceage');
const debit = new task1.DebitCard(500, 'Suslik Drovosek');

console.log(credit.refill(150));
console.log(credit.withdrawal(1300))
console.log(debit.withdrawal(600));
 */

/* console.log('===========task2==========');
const { SIZE } = require('./task2');
const Pizza = require('./task2');
const pizar = new Pizza(SIZE.big);
//pizar.addSize(Pizza.SIZE.small);
pizar.addTopping(Pizza.toppings.meat);
//pizar.addTopping(Pizza.toppings.salami);
pizar.addTopping(Pizza.toppings.greens);
//pizar.removeTopping(Pizza.toppings.salami);
pizar.addTopping(Pizza.toppings.cheese);
console.log(pizar.pizzaPrice());
console.log(pizar.pizzaCallories());
console.log(pizar.isVegan);
 */

console.log('===========task3==========');
const Track = require('./track');
const Album = require('./album');
const Artist = require('./artist');
const Library = require('./library')

const track1 = new Track('First Track', 100, true);
const track2 = new Track('Second Track', 100, true);
const track3 = new Track('Third Track', 100, true);
const track4 = new Track('Fourth Track', 150, false);
const alb1 = new Album('01.01.01', 'firstAlbum', [track1, track2, track3]);
const alb2 = new Album('02.02.02', 'Second Album', [track1, track2, track4]);
const svin = new Artist('Tus', 1895, [alb1, alb2], [track1, track2, track3, track4,]);



svin.addAlbum(alb1);
svin.addAlbum(alb2);
alb1.addTrack(track1);
alb1.addTrack(track2);
//alb1.addTrack(track3);
alb2.addTrack(track1);
alb2.addTrack(track2);
alb2.addTrack(track4);

//const libList = [];
const library = new Library();
library.add(svin);
library.add(track1);
library.add(track3);
library.add(track2);
library.update(track3, track4);
library.del(track3);

console.log(svin);
//console.log(alb1);
