class Pizza {

    static SIZE = {
        small: { name: 'small', price: 50, calories: 20 },
        big: { name: 'big', price: 100, calories: 40 }
    }
    isVegan = true;
    static toppings = {
        cheese: { name: 'cheese', price: 10, calories: 15 },
        salami: { name: 'salami', price: 15, calories: 20 },
        greens: { name: 'greens', price: 10, calories: 5 },
        meat: { name: 'meat', price: 20, calories: 15 }
    }
    constructor(SIZE) {
        this.SIZE = SIZE;
    }

    addTopping(toppings) {
        if (toppings.name === 'salami' || toppings.name === 'meat') {
            this.isVegan = false;
        }

        topList.push(toppings);
    }

    addSize(SIZE) {
        //this.SIZE = SIZE;
        topList.push(SIZE);
    }
    pizzaPrice() {
        this.topList = topList;
        let sum = 0;
        topList.map(topping => sum += +`${topping.price}`);
        sum += this.SIZE.price;

        return ('pizza price: ' + sum + ' rubles');
    }
    pizzaCallories() {
        this.topList = topList;
        let sum = 0;
        topList.map(topping => sum += +`${topping.calories}`);
        sum += +this.SIZE.calories;
        return ('pizza calories: ' + sum + ' cal');
    }

    removeTopping(toppings) {
        this.topList = topList;
        for (let i = 0; i < topList.length; i++) {
            if (topList[i] === toppings) {
                topList.splice(i, 1);
                break;
            }
        }
    }

}
let isVegan = true;
const topList = [];
module.exports = Pizza;