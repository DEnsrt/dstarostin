/* eslint-disable */
class Calculator {
  constructor() {}

  plus(a, b) {
    if (a != Number(a) || b != Number(b)) {
      throw new Error("NaN");
    }
    return a + b;
  }

  multiply(a, b) {
    if (a != Number(a) || b != Number(b)) {
      throw new Error("NaN");
    }
    return a * b;
  }

  divide(a, b) {
    if (b == 0) {
      throw new Error("Can't divide on 0!");
    }
    if (a != Number(a) || b != Number(b)) {
      throw new Error("NaN");
    }
    return a / b;
  }

  substract(a, b) {
    if (a != Number(a) || b != Number(b)) {
      throw new Error("NaN");
    }
    return a - b;
  }
}

const calc = new Calculator();
console.log(calc.plus(-10, 5));

module.exports = { Calculator };
