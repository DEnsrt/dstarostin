let loading = new Promise(function (resolve, reject) {
  let P = ["\\", "|", "/", "-"];
  let x = 0;
  return setInterval(function () {
    process.stdout.write("\r" + P[x++]);
    x &= 3;
    resolve();
  }, 250);
});

loading.then(() => { console.log('done') })

module.exports = loading;