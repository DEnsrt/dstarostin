console.log('===========task1==========');
const loading = require('./task1');
console.log(loading);


console.log('===========library extended==========');
const track = require('./track');
const album = require('./album');
const artist = require('./artist');
const Library = require('./library')
//const search = require('./hw06.search');

const track1 = new track('First Track', 10, true);
const track2 = new track('Second Track', 7, false);
const alb1 = new album('01.01.01', 'firstAlbum', track1);
const track3 = new track('Third Track', 6, true);
const track4 = new track('Fourth Track', 8, false);
const alb2 = new album('02.02.02', 'Second Album', [track3, track4])
const svin = new artist('Tus', 1895, alb1);

//console.log(svin);
const libList = [];
const library = new Library();
library.add(svin);
library.add(track1);
library.add(track2);
library.add(track3);
library.add(track4);
//library.update(track3, track4);
//library.del(track3);
//library.del(track4);
//console.log(library1);
//library.play();
/* setTimeout(() => {
    library.next();
}, 5000); */
/* setTimeout(() => {
    library.prev();
}, 9000); */

console.log(library.search(track2));
