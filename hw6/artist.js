class Artist {
    constructor(artistName, dateFound, album) {
        this.artistName = artistName;
        this.dateFound = dateFound;
        this.album = album;
    }
   
};

module.exports = Artist;