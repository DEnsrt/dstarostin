const axios = require("axios").default;

describe("comments API", () => {
  it("user email", async () => {
    const URL = "https://jsonplaceholder.typicode.com/comments/111";
    const result = await axios.get(URL);
    expect(result.data.email).toBe("Stefan.Crist@duane.ca");
  });

  it("Contain headers", async () => {
    const URL = "https://jsonplaceholder.typicode.com/comments/155";
    const result = await axios.get(URL);
    expect(result.headers).toEqual({
      "cache-control": "max-age=43200",
      "content-type": "application/json; charset=utf-8",
      expires: "-1",
      pragma: "no-cache",
    });
  });

  it("add new field", async () => {
    const URL = "https://jsonplaceholder.typicode.com/comments";
    const data = {
      name: "POST request",
      body: "this is POST request ",
      id: "666",
    };
    const result = await axios.post(URL, data);
    expect(result.status).toBe(201);
  });
});
