const axios = require("axios").default;

describe("photos API", () => {
  it("should update title", async () => {
    const URL = "https://jsonplaceholder.typicode.com/photos/3333";
    const data = { title: "this is a new TITLE" };
    const result = await axios.patch(URL, data);
    expect(result.data.title).toBe("this is a new TITLE");
    expect(result.status).toBe(200);
  });

  it("delete photo", async () => {
    const URL = "https://jsonplaceholder.typicode.com/photos/4444";
    const result = await axios.delete(URL);
    expect(result.status).toBe(200);
  });
});
