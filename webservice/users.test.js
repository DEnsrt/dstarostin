const axios = require("axios").default;

describe("users API", () => {
  it("show user number 5", async () => {
    const URL = "https://jsonplaceholder.typicode.com/users/5";
    const result = await axios.get(URL);
    expect(result.data.id).toBe(5);
  });
  it("create new user", async () => {
    const URL = "https://jsonplaceholder.typicode.com/users/";
    const data = {
      id: 11,
      name: "John Kartoshka",
      username: "JK",
      email: "kartoha@annie.ca",
      address: {
        street: "Evergreen Terrace",
        suite: "742",
        city: "Springfield",
        zipcode: "33263",
      },
      phone: "(123)789-4567",
      website: "not.com",
    };
    const result = await axios.post(URL, data);
    expect(result.status).toBe(201);
  });
});
