const axios = require("axios").default;

describe("todos API", () => {
  it("show the info about todos", async () => {
    const URL = "https://jsonplaceholder.typicode.com/todos";
    const result = await axios.get(URL);
    expect(result.headers).toBeTruthy();
  });

  it("show the info about todos", async () => {
    const URL = "https://jsonplaceholder.typicode.com/todos/55";
    const result = await axios.get(URL);
    expect(result.status).toBe(200);
    expect(result.data.completed).toBe(true);
  });
});
