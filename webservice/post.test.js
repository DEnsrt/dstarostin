const axios = require("axios").default;

describe("posts API", () => {
  it("id must be equal 5", async () => {
    let URL = "https://jsonplaceholder.typicode.com/posts/5";
    let result = await axios.get(URL);
    expect(result.data.id).toBe(5);
  });

  it("return success status pesponce", async () => {
    let URL = "https://jsonplaceholder.typicode.com/posts/";
    let result = await axios.get(URL);
    expect(result.status).toBe(200);
  });

  it("headers are present when requesting", async () => {
    let URL = "https://jsonplaceholder.typicode.com/posts/";
    let result = await axios.get(URL);
    expect(result.headers).toBeTruthy();
  });
});
