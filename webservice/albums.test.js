const axios = require("axios").default;

describe("albums API", () => {
  it("", async () => {
    const URL = "https://jsonplaceholder.typicode.com/albums";
    const result = await axios.get(URL);
    expect(result.data.length).toBe(100);
  });

  it("updates data", async () => {
    const URL = "https://jsonplaceholder.typicode.com/albums/99";

    const data = {
      userId: 1111,
      title: "new test text",
    };
    const result = await axios.put(URL, data);

    expect(result.status).toBe(200);
  });
  it("updates data", async () => {
    const URL = "https://jsonplaceholder.typicode.com/albums/99";

    const data = {
      userId: 1111,
      title: "new test text",
    };
    const result = await axios.put(URL, data);

    expect(result.data.title).toBe("new test text");
  });
});
