function multiplier(value) {
    let temp = value;
    multiplier = function () {
        let value = arguments[0] ? arguments[0] : 1;
        return temp = temp * value;
    };
    return multiplier();
};
multiplier(1);

module.exports = { multiplier };