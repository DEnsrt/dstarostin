const userList = [];

const card = function Card(first, last, balance) {
    this.name = {
        first,
        last
    };
    this.balance = balance;
};

const holderEu = function HolderEuro(user, coef) {
    this.user = user;
    this.coef = coef;
    user.balance = user.balance * coef;
    return user.balance;
};

const refPlus = function refillBalancePlus(user, amount) {
    this.user = user;
    this.amount = amount;
    user.balance = user.balance + amount;
    return user.balance;
};

const refMinus = function refillBalanceMinus(user, amount) {
    this.user = user;
    this.amount = amount;
    user.balance = user.balance - amount;
    return user.balance;
};


module.exports = { card, holderEu, refPlus, refMinus };