console.log('===========task1==========');
const task1 = require('./task1');
console.log(task1.multiplier(3));
console.log(task1.multiplier(5));
console.log(task1.multiplier(2));

console.log('===========task2==========');
const task2 = require('./task2');
const userList = [];
const ivan = new task2.card('Ivan', 'Ivanov', 1200);
userList.push(ivan);
const peter = new task2.card('Peter', 'Petrov', 1000);
userList.push(peter);
console.log(userList);
const holdEu = task2.holderEu(ivan, 0.25);
console.log(holdEu);
const refplus = task2.refPlus(peter, 170);
console.log(refplus);
const refminus = task2.refMinus(peter, 700);
console.log(refminus);



console.log('===========task3==========');
const task3 = require('./task3');
const Nissan = new task3.MyCar(1500, 10, 13);
console.log(Nissan.driveTo());
Nissan.refill(new task3.GasStat(1).gas);
console.log(Nissan.driveTo());
Nissan.refill(new task3.GasStat(1).gas);
console.log(Nissan.driveTo());
Nissan.refill(new task3.GasStat(2).gas);
console.log(Nissan.driveTo());
Nissan.refill(new task3.GasStat(5).gas);

