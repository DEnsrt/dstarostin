class Track {
    constructor(trackName, time, explicit) {
        this.trackName = trackName;
        this.time = time;
        this.explicit = explicit;
    };
};

module.exports = Track;