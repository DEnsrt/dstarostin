console.log('===========task1==========');
const calcul = require('./task1');
console.log(calcul);





console.log('===========library extended==========');
const Track = require('./track');
const Album = require('./album');
const Artist = require('./artist');
const Library = require('./library')


const track1 = new Track('First Track', 10, true);
const track2 = new Track('Second Track', 7, false);
const alb1 = new Album('01.01.01', 'firstAlbum', track1);
const track3 = new Track('Third Track', 6, true);
const track4 = new Track('Fourth Track', 8, false);
const alb2 = new Album('02.02.02', 'Second Album', [track3, track4])
const svin = new Artist('Tus', 1895, alb1);

//console.log(svin);
const libList = [];
const library = new Library();
//library.add(svin);
//library.add(track1);
library.add(track1);
library.add(track2);
library.add(track3);
library.add(track4);
//library.update(track3, track4);
//library.del(track3);
//library.del(track3);
//console.log(library);
library.play();

setTimeout(() => {
    library.next();
}, 5000);

setTimeout(() => {
    library.prev()
}, 9000);

