const EventEmitter = require('events');
const emitter = new EventEmitter();

function plus(a, b) {
    console.log('Calculating plus: ');
    emitter.emit('calculated', a + b);
    let result = +a + +b;
    console.log('result: ' + a + '+' + b + '=' + result);
}

function minus(a, b) {
    console.log('Calculating minus: ');
    emitter.emit('calculated', a - b);
    let result = a - b;
    console.log('result: ' + a + '-' + b + '=' + result);
}

function multiply(a, b) {
    console.log('Calculating multiply: ');
    emitter.emit('calculated', a * b);
    let result = a * b;
    console.log('result: ' + a + '*' + b + '=' + result);
}

function devide(a, b) {
    console.log('Calculating devide: ');
    emitter.emit('calculated', a / b);
    let result = a / b;
    console.log('result: ' + a + '/' + b + '=' + result);
}

function calculate(operation, a, b) {
    emitter.emit(operation, a, b);
}

emitter.on('cal', calculate);
emitter.on('+', plus);
emitter.on('-', minus);
emitter.on('*', multiply);
emitter.on('/', devide);

exports.emitter = emitter;


