class Album {
    constructor(albumDate, albumName, track) {
        this.albumName = albumName;
        this.albumDate = albumDate;
        this.track = track;
    };

};

module.exports = Album;