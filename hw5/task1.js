const fs = require('fs');
const readline = require('readline');
const { emitter } = require('./calculator');

const instream = fs.createReadStream('C:/Users/Dmitry/Desktop/epam/bitbucket/dstarostin/hw5/input.txt');

let rl = readline.createInterface(instream);


rl.on('line', function (line) {
    let string = line.split(' ');
    let a = string[0];
    let b = string[1];
    let c = string[2];
    emitter.emit('cal', c, a, b);
});

instream.on('end', function () {
    console.log('THE END!');
});

instream.on('error', function (err) {
    if (err.code == 'ENOENT') {
        console.log('File not found')
    } else {
        console.error(err)
    }
});

module.exports = { readline };