const Artist = require('./artist')

class ArtistFactory {
    create(artistName, dateFound, albumList, trackList) {
        return new Artist(artistName, dateFound, albumList, trackList)
    }
}

module.exports = ArtistFactory;