const Album = require('./album')
const artist = require('./artist')

class AlbumFactory {
    create(albumName, albumDate, trackList, artist) {
        return new Album(albumName, albumDate, trackList, artist)
    }
}
module.exports = AlbumFactory;
