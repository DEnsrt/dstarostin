class Track {

    constructor(trackName, time, explicit, album = [], artist = null) {
        this.trackName = trackName;
        this.time = time;
        this.explicit = explicit;
        this.album = album;
        this.artist = artist;
    };

    static isTrack(instance) {
        return instance instanceof Track
    }

};

module.exports = Track;