const Track = require("./track");
const Album = require("./album")
const evenEmmiter = require('events')
const repl = require('repl');
const Artist = require("./artist");
const AlbumFactory = require("./albumfactory");

class Library {
    constructor(trackList) {
        this.trackList = trackList || [];
        this.timerId = null;
        this.currentTrackIndex = 0;
        this.currentTrack = trackList[this.currentTrackIndex] || null;
        this.currentTime = 0;

        if (Library.exists) {
            return Library.instance
        }
        Library.instance = this;
        Library.exists = true;
    }

    add(instance, ...addit) {
        if (Track.isTrack(instance)) {
            this.trackList.push(instance);
        }
        if (Artist.isArtist(instance) && addit.every(Track.isTrack)) {
            instance.trackList.push(...addit);
        }
        if (Artist.isArtist(instance) && addit.every(Album.isAlbum)) {
            instance.albumList.push(...addit);
        }
        if (Album.isAlbum(instance) && addit.every(Track.isTrack)) {
            instance.trackList.push(...addit);
        }
        if (Album.isAlbum(instance) && Artist.isArtist(addit)) {
            instance.Artist = addit;
        }

    };
    del(instance) {

        if (Track.isTrack(instance)) {
            this.trackList.splice(this.trackList.indexOf(instance), 1);
        };
        if (Artist.isArtist(instance)) {
            this.trackList.forEach(element => {
                element.artist = null;
                element.album.artist = null;

            });

        };
        if (Album.isAlbum(instance)) {
            this.albumList.splice(this.albumList.indexOf(instance), 1);
        };
    };


    update(instance, property, newValue) {
        if (Track.isTrack(instance)) {
            instance[property] = newValue;
        };
        if (Album.isAlbum(instance)) {
            instance[property] = newValue;
        };
        if (Artist.isArtist(instance)) {
            instance[property] = newValue;
        }

    };

    play() {
        this.timerId = setInterval(() => {
            console.log(`playing ${this.currentTrack.trackName} on ${this.currentTime} sec`)
            //process.stdout.write('\r' + this.currentTime++);
            if (this.currentTrack.time <= this.currentTime) {
                this.next();
            };
            this.currentTime++;
        }, 1000);
    };

    next() {
        clearInterval(this.timerId);
        this.timerId = null;
        ++this.currentTrackIndex;
        this.currentTrack = this.trackList[this.currentTrackIndex];
        this.currentTime = 0;

        if (!this.currentTrack) {
            console.log('no more tracks, the end!');
            return;
        };
        console.log(`NEXT TRACK IS PLAYING! ${this.currentTrack.trackName}`);
        this.play();
    };

    prev() {
        clearInterval(this.timerId);
        this.timerId = null;
        this.currentTime = 0;
        --this.currentTrackIndex;
        this.currentTrack = this.trackList[this.currentTrackIndex];
        console.log(`PREV TRACK IS PLAYING! ${this.currentTrack.trackName}`);
        if (0 < this.currentTrackIndex < this.trackList.length) {
            this.play();
        };

    };

    pause() {
        clearInterval(this.timerId);
        console.log('paused, please, click  play to continue');
    };

    search(instance) {
        setTimeout(() => {
            if (Track.isTrack(instance) || Artist.isArtist(instance) || Album.isAlbum(instance)) {
                console.log(instance);
            };
        }, 300);
    };

};


module.exports = Library;