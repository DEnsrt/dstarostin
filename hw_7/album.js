class Album {
    constructor(albumName, albumDate, trackList = [], artist = null) {
        this.albumName = albumName;
        this.albumDate = albumDate;
        this.artist = artist;
        this.trackList = trackList;

        this.trackList.forEach((track) => {
            track.album = this;
            track.artist = artist;
        });
    };
    static isAlbum(instance) {
        return instance instanceof Album;
    }
    /* 
        addTrack(track) {
            this.trackList.push(track);
            track.artist = this.artist;
            track.album = this.album;
    
            this.trackList[this.trackList.length - 1].album = this;
        } */


}
module.exports = Album;