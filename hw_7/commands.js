class Command {
    constructor(context) {
        this.context = context;
    }
    execute() { }
}

class PlayCommand extends Command {
    constructor(library) {
        super(library);
    }
    execute() {
        this.context.play();
    }
};

class PauseCommand extends Command {
    constructor(library) {
        super(library);
    }
    execute() {
        this.context.pause();
    }
};
class NextCommand extends Command {
    constructor(library) {
        super(library);
    }
    execute() {
        this.context.next();
    }
};

class PrevCommand extends Command {
    constructor(library) {
        super(library);
    }
    execute() {
        this.context.prev();
    };
};



/* const server = repl.start(">");
Object.entries(commands).map(([name, cmd]) => {
    server.defineCommand(name, () => cmd.execute)
}); */
module.exports = { PlayCommand, PauseCommand, NextCommand, PrevCommand }