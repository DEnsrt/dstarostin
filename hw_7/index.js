/* console.log('===========task1==========');
const StringBuilder = require('./task1')
const helloBuilder = new StringBuilder("H");
helloBuilder.append("el");
helloBuilder.append("lo");
helloBuilder.delete(1, 3);
console.log(helloBuilder.build().toString()); */




console.log('===========library extended==========');
const track = require('./track');
const album = require('./album');
const artist = require('./artist');
const Library = require('./library')
const repl = require('repl')
const command = require('./commands')
const PlayCommand = command.PlayCommand;
const PauseCommand = command.PauseCommand;
const NextCommand = command.NextCommand;
const PrevCommand = command.PrevCommand;

const TrackFactory = require('./_trackfactory')
const AlbumFactory = require('./albumfactory')
const ArtistFactory = require('./artistfactory');
const Album = require('./album');
const Track = require('./track');
const artistfactory = new ArtistFactory();
const albumfactory = new AlbumFactory();
const trackfactory = new TrackFactory();

const albTrack1 = trackfactory.create('First alb Track', 9, true);
const albTrack2 = trackfactory.create('Second alb Track', 8, true);
const albTrack3 = trackfactory.create('Third alb Track', 7, true);
const albTrack4 = trackfactory.create('Fourth alb Track', 8, false);
const alb1 = albumfactory.create('First album', 1500, [albTrack1, albTrack2]);
const alb2 = albumfactory.create('Second album', 1600, [albTrack3, albTrack4]);
const alb3 = albumfactory.create('Third album', 1700);
const svin = artistfactory.create('Tus', 1895, [alb1, alb2]);


const library = new Library([albTrack1, albTrack2]);
library.add(albTrack3)

const commands = {
    play: new PlayCommand(library),
    next: new NextCommand(library),
    prev: new PrevCommand(library),
    pause: new PauseCommand(library)
};

const server = repl.start(">");
Object.entries(commands).map(([name, cmd]) => {
    server.defineCommand(name, () => cmd.execute())
});


