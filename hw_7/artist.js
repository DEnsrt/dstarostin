class Artist {

    constructor(artistName, dateFound, albumList = [], trackList = []) {
        this.artistName = artistName;
        this.dateFound = dateFound;
        this.albumList = albumList;
        this.trackList = trackList;

        this.albumList.forEach((album) => {
            album.artist = this;
            album.trackList.forEach((track) => {
                track.artist = this;
                this.trackList.push(track);
            });
        });
        this.trackList.forEach((track) => {
            track.artist = this;
        });
    }

    static isArtist(instance) {
        return instance instanceof Artist
    }




    /*  addAlbum(album) {
         this.albumList.push(album);
         this.albumList[this.albumList.length - 1].artist = this;
     } */
};

module.exports = Artist;