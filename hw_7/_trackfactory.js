const Track = require('./track')

class TrackFactory {
    create(trackName, time, explicit, album, artist) {
        return new Track(trackName, time, explicit, album, artist)
    }
}
module.exports = TrackFactory;


