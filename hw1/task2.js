function fibonacci(max) {
    const result = [
        0,
        1
    ];
    let value;
    for (let i = 1; ; i++) {
        value = result[i] + result[i - 1];
        if (value <= max)
            result.push(value);
        else break

    }
    return result;
}


module.exports = {fibonacci};