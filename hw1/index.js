console.log("===========task1==========");
const task1 = require("./task1"),
    factor = task1.factorial(100);
console.log(factor);


console.log("===========task2==========");
const task2 = require("./task2"),
    arr = task2.fibonacci(100);
console.log(arr);


console.log("===========task3==========");
const task3 = require("./task3"),
    rab = task3.rabbits();
console.log(rab);